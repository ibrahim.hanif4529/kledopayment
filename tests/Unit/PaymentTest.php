<?php

namespace Tests\Unit;

use App\Jobs\DeletePayment;
use App\Models\Payment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Support\Facades\Queue;

class PaymentTest extends TestCase
{
    use RefreshDatabase;

    public function test_load_api_index()
    {
        $response = $this->get('/api/payments');
        $response->assertStatus(200);
    }

    public function test_api_create_payment()
    {
        $response = $this->post('/api/payments', [
            'name' => "Nama"
        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(201);
    }

    public function test_api_create_with_no_valid_data()
    {
        $response = $this->post('/api/payments', [], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(422);
    }

    public function test_api_delete(){
        Queue::fake();
        Payment::factory(10)->create();
        $response = $this->delete('/api/payments', [
            'list_id'=>[1,2,4,5]
        ]);
        $response->assertStatus(200);
    }


    public function test_check_event(){
        Event::fake([
            \App\Events\DeletePayment::class
        ]);
        Payment::factory(10)->create();
        $event = new \App\Events\DeletePayment(false, 1);
        \event($event);
        Event::assertDispatched(\App\Events\DeletePayment::class);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_job_pushed()
    {
        Queue::fake();
        Payment::factory(10)->create();
        $job = new DeletePayment([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        dispatch($job);
        Queue::assertPushed(DeletePayment::class);
    }


}
