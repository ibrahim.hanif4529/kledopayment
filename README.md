# Installation
Please run this shell to perform installation
```shell script
composer install
```

# Configuration
Please configure .env data

```textmate
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=kledopayment
DB_USERNAME=root
DB_PASSWORD=
BROADCAST_DRIVER=pusher
CACHE_DRIVER=file
QUEUE_CONNECTION=database

PUSHER_APP_ID=1178874
PUSHER_APP_KEY=73b5ddd64fd282fa8c7f
PUSHER_APP_SECRET=a7996522cf2cf6d0f679
PUSHER_APP_CLUSTER=mt1
```
```shell script
php artisan migrate:fresh && php artisan db:seed
```

# Testing
For testing please run 
```shell script
php artisan test
```

# Running
For running please run 
```shell script
php artisan serve
```
Open in other terminal session
```shell script
php artisan queue:work
```
Open in other terminal session again
```shell script
php artisan queue:listen
```
