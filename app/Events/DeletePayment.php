<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DeletePayment implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $idPayment;
    protected $done;
    public $message;

    /**
     * Create a new event instance.
     *
     * @param bool $done
     * @param int $idPayment
     */
    public function __construct($done, $idPayment)
    {
        $this->done = $done;
        $this->idPayment = $idPayment;
        if ($this->done) {
            $this->message = "Hapus data selesai";
        } else {
            $this->message = "Berhasil menghapus payment dengan id: " . $this->idPayment;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('delete-payment');
    }


}
