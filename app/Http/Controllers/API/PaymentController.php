<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentFormRequest;
use App\Http\Resources\PaymentCollection;
use App\Jobs\DeletePayment;
use App\Models\Payment;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return PaymentCollection
     */
    public function index()
    {

        $payments = Payment::orderBy('created_at', 'desc')->paginate(5);
        return $payments->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PaymentFormRequest $request
     * @return JsonResponse
     */
    public function store(PaymentFormRequest $request)
    {
        $request->validated();
        try {
            $data = $request->all();
            $payment = Payment::create($data);
            return response()->json($payment, 201);
        }catch (Exception $exception){
            return response()->json([
                'error'=> $exception->getMessage()
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        $list_id = $request->list_id;
        $job = new DeletePayment($list_id);
        dispatch($job);
        return \response()->json($request->all());
    }
}
