<?php

namespace App\Jobs;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class DeletePayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $list_id;

    /**
     * Create a new job instance.
     * @param array $list_id
     * @return void
     */
    public function __construct($list_id)
    {
        $this->list_id = $list_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->list_id as  $id){
            $payment = Payment::find($id);
            if(!is_null($payment)){
                $payment->delete();
                Log::alert("Message delete".$id. "/".end($this->list_id));
                $isDone = $id == end($this->list_id);
                event(new \App\Events\DeletePayment($isDone, $id));
            }
        }
    }

}
